#ifndef PORTIO_H_
#define PORTIO_H_
/*
 * PortIO.h
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Jun 21, 2015
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */


unsigned short XCOutputScanLinePtr( const unsigned int* const i_Mem, unsigned int i_PixelCount, unsigned short i_AtTime );

unsigned short XCOutputPixel( const unsigned int i_PackedColor, unsigned short i_AtTime );


#endif // PORTIO_H_
