#ifndef GLOBALS_H_
#define GLOBALS_H_
/*
 * Globals.h
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Jun 21, 2015
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */


#define ScanLineBufferCount 4

//#define EnablePS2Log
//#define EnableScopeTrig

#define PS2DeviceCount 1

#define PS2_LO 1
#define PS2_HI 0
#define PS2_INV 1

//#define MouseEventMax 16
//#define KeybEventMax 16

#endif // GLOBALS_H_
