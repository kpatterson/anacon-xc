/*
 * FastIntSqrt.c - Based on code by Ryan Geiss from http://www.geisswerks.com/ryan/FAQS/fast_int_sqrt.html
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Mar 1, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include <math.h>


unsigned char int_sqrt_bits[64] = {
    2, 4, 4, 4, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
    8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
    8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
    8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
};

unsigned short int_sqrt_x1024[4096];


void init_int_sqrt() {
    for( int i = 0; i < 4096; ++i )
        int_sqrt_x1024[i] = sqrt( i + 0.5f ) * 1024.0f;
}

unsigned short int_sqrt(unsigned int x)
{
    // This *integer* sqrt functiona is fast, 100% accurate, and uses 8,256 bytes of lookup tables.
    // For input values below 2^24, it takes about 25(?) cycles.
    // For input values above 2^24, it has to do an additional 5 conditional tests, to fix-up the value,
    //     so for those, it's slower.
    //
    // Storage:
    //     unsigned char int_sqrt_bits[64] =
    //     {
    //       2, 4, 4, 4, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
    //       8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
    //       8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
    //       8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
    //     };
    //     unsigned short int_sqrt_x1024[4096];
    //     bool int_sqrt_x1024_READY = false;
    //
    //
    // Init:
    //     for (int i=0; i<sizeof(int_sqrt_x1024)/sizeof(int_sqrt_x1024[0]); i++)
    //         int_sqrt_x1024[i] = (int)(sqrtf((float)i + 0.5f) * 1024.0f);
    //     int_sqrt_x1024_READY = true;
    //
    // Evaluation:
    //     LUT size: [0..4095]       (max input = 12 bits)
    //     LUT type: unsigned short  (16 bits)
    //     LUT memory: 8 KB
    //     input:    the integer you want the sqrt for
    //     contents:   the sqrtf() of the input value, multiplied by 1024 (~precision-enhanced), and then cast to unsigned short.
    //     Using the lookup table:
    //       If input is...  then...
    //       < 2^12:         take LUT[input >>  0] >> 10
    //       < 2^14:         take LUT[input >>  2] >> 9
    //       < 2^16:         take LUT[input >>  4] >> 8
    //       < 2^18:         take LUT[input >>  6] >> 7
    //       < 2^20:         take LUT[input >>  8] >> 6
    //       < 2^22:         take LUT[input >> 10] >> 5
    //       < 2^24:         take LUT[input >> 12] >> 4
    //       < 2^26:         take LUT[input >> 14] >> 3
    //       < 2^28:         take LUT[input >> 16] >> 2
    //       < 2^30:         take LUT[input >> 18] >> 1
    //       < 2^32:         take LUT[input >> 20] >> 0

    unsigned char b;
    {
        // returns: (max(12, # of bits required to represent the value of x) + 1) & 1
        unsigned int t, tt; // temporaries
        if( (tt = (x >> 16)) )
        {
            if( (t = (tt >> 8)) )
            {
                b = 24 + int_sqrt_bits[t>>2];

                unsigned int ret = (unsigned int)( int_sqrt_x1024[ x >> (b-12) ] >> ((32-b)>>1) );
                // Note: after using the LUT and shifting properly, the answer can be off by:
                //   input in [0    .. 2^24-1] ->  output is perfect
                //   input in [2^24 .. 2^26-1] ->  output off by [-1..0]
                //   input in [2^26 .. 2^28-1] ->  output off by [-2..1]
                //   input in [2^28 .. 2^30-1] ->  output off by [-4..3]
                //   input in [2^30 .. 2^32-1] ->  output off by [-8..7]
                // (TODO: could probably remove some of these conditionals by using a larger LUT.)

                // so, we fix it up:
                if (ret > 65535-7)  // protect vs. overflow of ret*ret.
                    ret = 65535-7;
                //OPTIMIZE?
                ret += (x >= ret*ret) ? 4 : -4;
                ret += (x >= ret*ret) ? 2 : -2;
                ret += (x >= ret*ret) ? 1 : -1;
                ret += (x >= ret*ret) ? 0 : -1;

                return ret;
            }
            else
                b = 16 + int_sqrt_bits[tt>>2];
        }
        else
            b = (t = (x >> 12)) ? 12 + int_sqrt_bits[t>>2] : 12;
    }

    unsigned int ret = (unsigned int)( int_sqrt_x1024[ x >> (b-12) ] >> ((32-b)>>1) );

    // minor fix-up; this is needed because we create our LUT with i+0.5f.
    // (...that gains us a balanced error for our _LOOSE version, so we don't have to
    //     add a bias of +1, +2, +4, or +8 depending on the # of bits.  We save cycles
    //     where we want them (_LOOSE), and we add a few cycles for the 100% accurate
    //     (and probably very rarely-needed) version of the function.
    ret += (x >= ret*ret) ? 0 : -1;

    return ret;
}

unsigned int int_sqrt_LOOSE( unsigned int x ) {
    // Same as int_sqrt(), but return value can have some error.
    //   input in [0    .. 2^24-1] ->  output is perfect
    //   input in [2^24 .. 2^26-1] ->  output off by [-1..0]
    //   input in [2^26 .. 2^28-1] ->  output off by [-2..1]
    //   input in [2^28 .. 2^30-1] ->  output off by [-4..3]
    //   input in [2^30 .. 2^32-1] ->  output off by [-8..7]
    // Note that that's 0.024% (1/4096) - not 2.4%!!

    unsigned int b;
    {
        // returns: (max(12, # of bits required to represent the value of x) + 1) & 1
        unsigned int t, tt; // temporaries
        if( (tt = (x >> 16)) ) {
            if( (t = (tt >> 8)) )
                b = 24 + int_sqrt_bits[t >> 2];
            else
                b = 16 + int_sqrt_bits[tt >> 2];
        }
        else
            b = (t = (x >> 12)) ? 12 + int_sqrt_bits[t >> 2] : 12;
    }

    unsigned int ret = int_sqrt_x1024[x >> (b - 12)] >> ((32 - b) >> 1);

//  ret += (x >= ret*ret) ? 0 : -1;

    return ret;
}
