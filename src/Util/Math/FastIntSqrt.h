#ifndef FASTINTSQRT_H_
#define FASTINTSQRT_H_
/*
 * FastIntSqrt.h - Based on code by Ryan Geiss from http://www.geisswerks.com/ryan/FAQS/fast_int_sqrt.html
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Mar 1, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

extern void init_int_sqrt();
extern unsigned short int_sqrt( unsigned int x );
extern unsigned int int_sqrt_LOOSE( unsigned int x );


#endif /* FASTINTSQRT_H_ */
