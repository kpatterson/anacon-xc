/*
 * Main.xc
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Feb 7, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright � 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

extern "C" {
    #include "CppMain.h"
    #include "Util/Math/FastIntSqrt.h"
    #include "Grafx/Palette/Palette.h"
    #include "Grafx/CharBitmap/CharBitmap.h"
}

#include "VideoOut.xc.h"
#include "UserInput/PS2/PS2.xc.h"


clock Clk = XS1_CLKBLK_REF;


int main( void ) {
    configure_out_port( VideoDAC, Clk, 0 );
    configure_out_port( DACClock, Clk, 0 );
    configure_out_port( VideoHSync, Clk, 1 );
    configure_out_port( VideoVSync, Clk, 1 );

    init_int_sqrt();

    InitPalette();
    InitCharBitmap();

    start_clock( Clk );

    VideoOutInit();

    CppInit();

    PS2Init( Clk );

    chan PS2Chan;

    par {
        VideoOutLoop( PS2Chan );
        PS2Loop( PS2Chan );
    }

    return 0;
}
