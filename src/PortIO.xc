/*
 * PortIO.xc
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Jun 21, 2015
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

extern "C" {
    #include "Globals.h"
    #include "Grafx/Palette/Palette.h"
}

#include "VideoOut.xc.h"


unsigned short XCOutputPixel( const unsigned int i_PackedColor, unsigned short i_AtTime ) {
    DACClock <: 0;
    VideoDAC @ i_AtTime <: i_PackedColor;
    DACClock @ i_AtTime + 3 <: 1;
    return i_AtTime + 6;
}


unsigned short XCOutputScanLinePtr( const unsigned int i_Mem[i_PixelCount], unsigned int i_PixelCount, unsigned short i_AtTime ) {
    unsigned int FracClock = 0;

    DACClock <: 0;

    for( unsigned int x = 0; x < i_PixelCount; ++x ) {
        VideoDAC @ i_AtTime <: i_Mem[x];
            FracClock += 120;    //96 min; 96 : 400px, 120 : 320px
        DACClock @ i_AtTime + 3 <: 1;
            const unsigned int Delta = FracClock >> 3;
        DACClock @ i_AtTime + 6 <: 0;
            i_AtTime += Delta;
            FracClock -= Delta << 3;
    }

    VideoDAC @ i_AtTime <: Border;
    DACClock @ i_AtTime + 3 <: 1;
    DACClock @ i_AtTime + 6 <: 0;

    return i_AtTime;
}

// double-scan for VGA (progressive)

//    for( unsigned int x = 0; x < 512; ++x ) {
//        VideoDAC @ i_AtTime <: Buf.p[x];
//            FracClock += 70;
//        DACClock /* @ i_AtTime + 3 */ <: 1;
//            const unsigned int Delta = FracClock >> 3;
//        DACClock /* @ i_AtTime + 6 */ <: 0;
//            i_AtTime += 9;//Delta;
//            FracClock -= Delta << 3;
//    }
