#ifndef PALETTE_H_
#define PALETTE_H_
/*
 * Grafx/Palette/Palette.h
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Jun 18, 2015
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */


#define PaletteSize 256


extern unsigned int Blank;
extern unsigned int Black;
extern unsigned int White;
extern unsigned int ColorBurst;
extern unsigned int Border;


void InitPalette();

unsigned int CGetColorAsRawPixel( unsigned int i_Color );


#endif // PALETTE_H_
