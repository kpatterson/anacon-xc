/*
 * Grafx/Palette/Palette.cpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Jun 16, 2015
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include "Grafx/Palette/Palette.hpp"

#include "Grafx/Pixel.hpp"
#include "Grafx/Screen.hpp"


unsigned int Palette[PaletteSize] = {
    // text
    0x00000000, // black
    0x00ffffff, // solid white

    // pinky pie
    0x00c8a1ed, // light pink
    0x00fbc64f, // blue
    0x006b31e1, // dark pink

    // rainbow dash
    0x00fbc974, // blue
    0x0070f8fe, // yellow
    0x00222de0, // red

    // twilight sparkle
    0x00dd86c2, // lavender
    0x006d31e1, // pink
    0x00561f14, // darp purple

    // fluttershy
    0x0068f8ff, // yellow
    0x006c8438, // teal
    0x00bc7de8, // light pink

    // rarity
    0x00ebe7e1, // gray
    0x00be5928, // blue
    0x0089203c, // dark violet

    // apple jack
    0x003a95ee, // orange
    0x003fb154, // green
    0x006ef9ff, // yellow

    // ghost pinky pie
    0x40c8a1ed, // light pink
    0x00fbc64f, // blue
    0x006b31e1, // dark pink

};


unsigned int Blank;
unsigned int Black;
unsigned int White;
unsigned int ColorBurst;
unsigned int Border;


extern "C" {

    void InitPalette() {
        Pixel* const PixPal = reinterpret_cast<Pixel* const>( Palette );

        // adjust colors for NTSC pedestal
        for( unsigned int i = 0; i < PaletteSize; ++i )
            PixPal[i].AddPedestal();

        Blank = 0;
        Black = Screen::PackPixel( GetColorAsRawPixel( 0 ) );
        White = Screen::PackPixel( GetColorAsRawPixel( 1 ) );
        ColorBurst = Blank;
        Border = Screen::PackPixel( GetColorAsRawPixel( 3 ) );
    }

    unsigned int CGetColorAsRawPixel( unsigned int i_Color ) {
        return GetColorAsRawPixel( i_Color );
    }
}
