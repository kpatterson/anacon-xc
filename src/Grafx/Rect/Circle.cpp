/*
 * Grafx/Rect/Circle.cpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Mar 1, 2017
 *      Author: Kevin H. Patterson
  *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include "Circle.hpp"

#include "Grafx/Pixel.hpp"

extern "C" {
    #include "Util/Math/FastIntSqrt.h"
}

#include <stdlib.h>


Circle::Circle( short i_X, short i_Y, unsigned short i_InnerRadius, unsigned short i_OuterRadius, Pixel i_Color )
: Rect( i_Y - i_OuterRadius, i_Y + i_OuterRadius + 1 )
, m_Xbegin( i_X - i_OuterRadius )
, m_Xend( i_X + i_OuterRadius + 1 )
, m_CenterOffset( i_OuterRadius )
, m_InnerRadius( i_InnerRadius << 8 )
, m_OuterRadius( i_OuterRadius << 8 )
, m_InnerRadiusSqMin( m_InnerRadius * m_InnerRadius )
, m_InnerRadiusSqMax( (m_InnerRadius + 256) * (m_InnerRadius + 256) )
, m_OuterRadiusSqMin( m_OuterRadius * m_OuterRadius )
, m_OuterRadiusSqMax( (m_OuterRadius + 256) * (m_OuterRadius + 256) )
, m_Color( i_Color )
{}

void Circle::Render( short i_Y, LineBuf& io_LineBuf ) {
    const int t_XCenter = m_Xbegin + m_CenterOffset;
    const int t_YCenter = Ybegin + m_CenterOffset;
    const int t_YDiff = (i_Y - t_YCenter) << 8;
    const unsigned int t_YDiffSq = t_YDiff * t_YDiff;

    int xStart = m_Xbegin;

    if( xStart < 0 )
        xStart = 0;

    if( xStart >= io_LineBuf.Width() )
        return;

    int xStop = m_Xend;

    if( xStop <= 0 )
        return;

    if( xStop > io_LineBuf.Width() )
        xStop = io_LineBuf.Width();

    //TODO: We can optimize the following loop by pre-calculating the X ranges (left and right sides of the circle) that will be drawn, instead of testing every X

    Pixel* t_Pixels = io_LineBuf.GetPixels();
    for( unsigned int x = xStart; x < xStop; ++x ) {
        const int t_XDiff = (x - t_XCenter) << 8;
        const unsigned int t_XDiffSq = t_XDiff * t_XDiff;
        const unsigned int t_DistSq = t_XDiffSq + t_YDiffSq;

        if( t_DistSq >= m_OuterRadiusSqMax || t_DistSq <= m_InnerRadiusSqMin )
            continue;

        if( t_DistSq <= m_OuterRadiusSqMin && t_DistSq >= m_InnerRadiusSqMax ) {
            t_Pixels[x] = m_Color;
            continue;
        }

        {
            const int t_Dist = int_sqrt_LOOSE( t_DistSq );
            const int a = t_Dist - m_OuterRadius;
            const int b = m_InnerRadius - t_Dist;
            const int c = (a > 0) ? a : b;
            t_Pixels[x].Blend( m_Color.raw | (c << 24) );
        }
    }
}
