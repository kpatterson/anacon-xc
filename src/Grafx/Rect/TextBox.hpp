#ifndef TEXTBOX_HPP_
#define TEXTBOX_HPP_
/*
 * Grafx/Rect/TextBox.hpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Jun 15, 2015
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include "Grafx/Rect/Rect.hpp"


class TextBox: public Rect {
public:
    TextBox( short i_X, short i_Y, unsigned short i_Cols, unsigned short i_Rows );
    virtual ~TextBox();

    virtual void Render( short i_Y, LineBuf& io_LineBuf );

    void DisplayInt( short i_Col, short i_Row, int i_Num );
    void DisplayString( short i_Col, short i_Row, const char i_StringZ[] );
    void DisplayStringR( short i_Col, short i_Row, const char i_StringZ[] );

protected:
    short m_X;

    unsigned short m_Cols;
    unsigned short m_Rows;

    unsigned char* m_CharMem;
    unsigned char* m_CharColors;
};

#endif // TEXTBOX_HPP_
