/*
 * Grafx/Rect/TextBox.cpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Jun 16, 2015
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include "Grafx/Rect/TextBox.hpp"

#include "Grafx/Pixel.hpp"
#include "Grafx/CharBitmap/CharBitmap.hpp"
#include "Grafx/Palette/Palette.hpp"


TextBox::TextBox( short i_X, short i_Y, unsigned short i_Cols, unsigned short i_Rows )
: Rect( i_Y, i_Y + i_Rows * 8 )
, m_X( i_X )
, m_Cols( i_Cols )
, m_Rows( i_Rows )
, m_CharMem( 0 )
, m_CharColors( 0 )
{
    m_CharMem = new unsigned char[m_Rows * m_Cols];
    m_CharColors = new unsigned char[m_Rows * m_Cols];
    for( unsigned int i = 0; i < m_Rows * m_Cols; ++i ) {
        m_CharMem[i] = 0;
        m_CharColors[i] = 0;
    }
}

TextBox::~TextBox() {
    delete[] m_CharColors;
    delete[] m_CharMem;
}


void TextBox::DisplayInt( short i_Col, short i_Row, int i_Num ) {
    if( i_Col < 0 )
        i_Col += m_Cols;

    if( i_Row < 0 )
        i_Row += m_Rows;

    if( i_Col < 0 || i_Col >= m_Cols || i_Row < 0 || i_Row >= m_Rows )
        return;

    bool s = false;
    if( i_Num < 0 ) {
        i_Num = -i_Num;
        s = true;
    }
    char c[12];
    c[0] = '0';
    int l = 0;
    while( i_Num ) {
        c[l] = (i_Num % 10) + 48;
        i_Num /= 10;
        ++l;
    }
    if( s ) {
        c[l] = '-';
        ++l;
    }
    if( l == 0 )
        ++l;
    c[l] = 0;
    DisplayStringR( i_Col, i_Row, c );
}

void TextBox::DisplayString( short i_Col, short i_Row, const char i_StringZ[] ) {
    if( i_Col < 0 )
        i_Col += m_Cols;

    if( i_Row < 0 )
        i_Row += m_Rows;

    if( i_Col < 0 || i_Col >= m_Cols || i_Row < 0 || i_Row >= m_Rows )
        return;

    int p = i_Row * m_Cols + i_Col;
    int i = 0;
    unsigned char c = i_StringZ[i];
    while( c ) {
        m_CharMem[p] = c;
        ++i;
        ++p;
        c = i_StringZ[i];
    };
}

void TextBox::DisplayStringR( short i_Col, short i_Row, const char i_StringZ[] ) {
    if( i_Col < 0 )
        i_Col += m_Cols;

    if( i_Row < 0 )
        i_Row += m_Rows;

    if( i_Col < 0 || i_Col >= m_Cols || i_Row < 0 || i_Row >= m_Rows )
        return;

    int p = i_Row * m_Cols + i_Col;
    int i = 0;
    unsigned char c = i_StringZ[i];
    while( c ) {
        m_CharMem[p] = c;
        ++i;
        --p;
        c = i_StringZ[i];
    };
}

void TextBox::Render( short i_Y, LineBuf& io_LineBuf ) {
    int t_Y = i_Y - Ybegin;

//    t_Y += i_Field;

    const int Offset = (t_Y >> 3) * m_Cols;
    const unsigned int* i_CharMem = reinterpret_cast<unsigned int*>( m_CharMem + Offset );
    const unsigned int* i_CharColor = reinterpret_cast<unsigned int*>( m_CharColors + Offset );

    const unsigned short* i_CharBitmap = CharBitmap + (t_Y & 7);

    Pixel* Pix = io_LineBuf.GetPixels() + m_X;
//        const Pixel* BkgPix = reinterpret_cast<Pixel*>( ScanLineBuf[i_ScanLineBkgBufNum].p );

//        unsigned int x = 0;
    for( unsigned int c = 0; c < (m_Cols >> 2); ++c ) {
        unsigned int color4 = i_CharColor[c];
        unsigned int char4 = i_CharMem[c];

//          Pix[x].raw = Pixel::FastBlend2( Palette[color + (bits & 0b11)], BkgPix[x].raw );
        {
            unsigned int bits = i_CharBitmap[(char4 & 0xff) << 3];
            const unsigned int color = color4 & 0xff;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)];
        }
        char4 >>= 8;
        color4 >>= 8;
        {
            unsigned int bits = i_CharBitmap[(char4 & 0xff) << 3];
            const unsigned int color = color4 & 0xff;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)];
        }
        char4 >>= 8;
        color4 >>= 8;
        {
            unsigned int bits = i_CharBitmap[(char4 & 0xff) << 3];
            const unsigned int color = color4 & 0xff;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)];
        }
        char4 >>= 8;
        color4 >>= 8;
        {
            unsigned int bits = i_CharBitmap[(char4 & 0xff) << 3];
            const unsigned int color = color4 & 0xff;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)]; bits >>= 2;
            Pix++->raw = Palette[color + (bits & 0b11)];
        }
    }
}
