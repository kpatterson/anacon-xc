/*
 * Grafx/Rect/Canvas.cpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Feb 1, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include "Grafx/Rect/Canvas.hpp"


void Canvas::PreRender( Screen& i_Screen ) {
    for( RectList_t::iterator it = m_Rects.begin(); it != m_Rects.end(); ++it )
        (*it)->PreRender( i_Screen );
}

void Canvas::Render( short i_Y, LineBuf& io_LineBuf ) {
    //TODO: Optimize this search for Rects that intersect with the current scanline
    for( RectList_t::iterator it = m_Rects.begin(); it != m_Rects.end(); ++it ) {
        Rect& R = **it;
        if( R.Ybegin <= i_Y && R.Yend > i_Y )
            R.Render( i_Y, io_LineBuf );
    }
}
