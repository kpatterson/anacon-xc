#ifndef RECT_HPP_
#define RECT_HPP_
/*
 * Grafx/Rect/Rect.hpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Feb 1, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include "Grafx/LineBuf.hpp"


class Screen;


class Rect {
public:
    Rect()
    : Ybegin( 0 )
    , Yend( 0 )
    {}

    Rect( short i_Ybegin, short i_Yend )
    : Ybegin( i_Ybegin )
    , Yend( i_Yend )
    {}

    virtual ~Rect() {}

    // called once per field or frame; Ybegin and Yend members must be updated inside
    virtual void PreRender( Screen& i_Screen ) {}

    // called once per scanline for all lines where Ybegin <= i_Y < Yend
    virtual void Render( short i_Y, LineBuf& io_LineBuf ) = 0;

public:
    short Ybegin;
    short Yend;
};

#endif /* RECT_HPP_ */
