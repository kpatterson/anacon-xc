#ifndef SPRITEAREA_HPP_
#define SPRITEAREA_HPP_
/*
 * Grafx/Rect/SpriteArea.hpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Jun 16, 2015
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include "Grafx/Rect/Rect.hpp"


class Sprite {
public:
    Sprite()
    : x( 0 )
    , y( 0 )
    , xd( 0 )
    , yd( 0 )
    , bitmap( 0 )
    , color( 0 )
    , flags()
    {}

private:
    friend class SpriteArea;

    short x;
    short y;
    short xd;
    short yd;
    unsigned char bitmap;
    unsigned char color;

    struct SpriteFlags {
        SpriteFlags()
        : active( false )
        , large( true )
        , magnify( false )
        , solid( false )
        {}

        bool active : 1;
        bool large : 1;
        bool magnify: 1;
        bool solid: 1;
    } flags;
};


class SpriteArea: public Rect
{
public:
    SpriteArea( short i_X, short i_Y, unsigned short i_Width, unsigned short i_Height, unsigned char i_MaxSprites, bool i_HalfLine = true, unsigned char i_DeltaPower = 4 );

    virtual ~SpriteArea();

    virtual void PreRender( Screen& i_Screen );

    virtual void Render( short i_Y, LineBuf& io_LineBuf );

    void SetSpriteCoords( unsigned char i_SpriteIndex, short i_x, short i_y );

    Sprite& GetSprite( unsigned char i_SpriteIndex ) { return S[i_SpriteIndex]; }

protected:
    const unsigned char Max;
    const unsigned char DeltaPower;
    const unsigned short xsize;
    const unsigned short ysize;

    short m_X;
    unsigned short m_Width;

    Sprite* S;

    bool m_HalfLine;
    unsigned char m_CurrentField;
};


#endif // SPRITEAREA_HPP_
