#ifndef CANVAS_HPP_
#define CANVAS_HPP_
/*
 * Grafx/Rect/Canvas.hpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Feb 1, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include "Grafx/Rect/Rect.hpp"

#include <list>


class Canvas
: public Rect
{
public:
    Canvas( short i_X, short i_Y, unsigned short i_Width, unsigned short i_Height )
    : Rect( i_Y, i_Y + i_Height )
//    , m_Xbegin( i_X )
//    , m_Xend( i_X + i_Width )
    {}

    virtual ~Canvas() {}

    virtual void PreRender( Screen& i_Screen );

    virtual void Render( short i_Y, LineBuf& io_LineBuf );

    typedef std::list<Rect*> RectList_t;

    RectList_t& GetRects() { return m_Rects; }

protected:
//    short m_Xbegin;
//    short m_Xend;
    RectList_t m_Rects;
};

#endif // CANVAS_HPP_
