/*
 * Grafx/Rect/SpriteArea.cpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Jun 16, 2015
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include "SpriteArea.hpp"

#include "Grafx/Pixel.hpp"
#include "Grafx/CharBitmap/CharBitmap.hpp"
#include "Grafx/Palette/Palette.hpp"
#include "Grafx/Screen.hpp"


SpriteArea::SpriteArea( short i_X, short i_Y, unsigned short i_Width, unsigned short i_Height, unsigned char i_MaxSprites, bool i_HalfLine, unsigned char i_DeltaPower )
: Rect( i_Y, i_Y + i_Height )
, Max( i_MaxSprites )
, DeltaPower( i_DeltaPower )
, xsize( i_Width << (DeltaPower + 1) )
, ysize( i_Height << (DeltaPower + 1) )
, m_X( i_X )
, m_Width( i_Width )
, m_HalfLine( i_HalfLine )
, m_CurrentField( 0 )
{
    S = new Sprite[Max];

    if( DeltaPower > 2 ) {
        for( unsigned int i = 0; i < Max; ++i ) {
            S[i].x = i << (DeltaPower - 2);
            S[i].y = i << (DeltaPower - 2);
        }
    }

    S[0].xd =   0; S[0].yd =  0; S[0].flags.active = true; S[0].y = 2200; S[0].color = 2;
    S[1].xd = -12; S[1].yd =  2; S[1].flags.active = true; S[1].y = 2400; S[1].color = 5;
    S[2].xd = -14; S[2].yd =  1; S[2].flags.active = true; S[2].y = 2600; S[2].color = 8;
    S[3].xd = -16; S[3].yd =  1; S[3].flags.active = true; S[3].y = 2800; S[3].color = 11;
    S[4].xd = -18; S[4].yd = -1; S[4].flags.active = true; S[4].y = 3000; S[4].flags.magnify = true; S[4].color = 20;
    S[5].xd = -20; S[5].yd = -2; S[5].flags.active = true; S[5].y = 3200; S[5].color = 14;
    S[6].xd = -22; S[6].yd = -4; S[6].flags.active = true; S[6].y = 3400; S[6].color = 17;
//    S[7].xd = -18; S[7].yd =  3; S[7].flags.active = true; S[7].y = 3600; S[7].color = 1;
}

SpriteArea::~SpriteArea() {
    delete[] S;
}

void SpriteArea::PreRender( Screen& i_Screen ) {
    m_CurrentField = i_Screen.CurrentField();

    for( unsigned int i = 0; i < Max; ++i ) {
        Sprite& Sp = S[i];

        int size = 8 << (DeltaPower + 1);
        if( Sp.flags.large )
            size <<= 1;
        if( Sp.flags.magnify )
            size <<= 1;

        int x = Sp.x;
        x += Sp.xd;
        int y = Sp.y;
        y += Sp.yd;

        if( x <= -size )
            x += xsize + size - 1;
        else if( x > xsize )
            x -= xsize + size;

        if( y <= -size )
            y += ysize + size - 1;
        else if( y > ysize )
            y -= ysize + size;

        Sp.x = x;
        Sp.y = y;
    }
}

void SpriteArea::SetSpriteCoords( unsigned char i_SpriteIndex, short i_x, short i_y ) {
    S[i_SpriteIndex].x = i_x << DeltaPower;
    S[i_SpriteIndex].y = i_y << DeltaPower;
}

void SpriteArea::Render( short i_Y, LineBuf& io_LineBuf ) {
    Pixel* const Buf = io_LineBuf.GetPixels() + m_X;

    const unsigned short* const i_CharBitmap = SpriteBitmap;

    int yOfs;

    for( int i = Max - 1; i >= 0; --i ) {
        const Sprite& Sp = S[i];

        if( m_HalfLine )
            yOfs = i_Y - Ybegin - (((Sp.y >> DeltaPower) + (m_CurrentField ^ 1)) >> 1);
        else
            yOfs = (i_Y - Ybegin - (Sp.y >> DeltaPower)) >> 1;

        if( Sp.flags.active && yOfs >= 0 ) {
            int x = Sp.x >> (DeltaPower + 1);
            const unsigned int* const Pal = Palette + Sp.color - 1 + Sp.flags.solid;
            const unsigned int Solid = Sp.flags.solid;

            if( Sp.flags.magnify ) {
                yOfs >>= 1;
                if( Sp.flags.large ) {
                    if( yOfs < 16 ) {
                        int CharOfs = (Sp.bitmap << 3) + yOfs;
                        if( yOfs >= 8 )
                            CharOfs += 8;

                        unsigned short bits = i_CharBitmap[CharOfs];
                        for( unsigned int b = 0; b < 8; ++b ) {
                            unsigned int color = bits & 0b11;
                            if( color | Solid ) {
                                if( x >=0 && x < m_Width )
                                    Buf[x].FastBlend2Src( Pal[color] );
                                ++x;
                                if( x >=0 && x < m_Width )
                                    Buf[x].FastBlend2Src( Pal[color] );
                                ++x;
                            } else
                                x += 2;
                            bits >>= 2;
                        }
                        bits = i_CharBitmap[CharOfs + 8];
                        for( unsigned int b = 0; b < 8; ++b ) {
                            unsigned int color = bits & 0b11;
                            if( color | Solid ) {
                                if( x >=0 && x < m_Width )
                                    Buf[x].FastBlend2Src( Pal[color] );
                                ++x;
                                if( x >=0 && x < m_Width )
                                    Buf[x].FastBlend2Src( Pal[color] );
                                ++x;
                            } else
                                x += 2;
                            bits >>= 2;
                        }
                    }
                } else {
                    if( yOfs < 8 ) {
                        unsigned short bits = i_CharBitmap[(Sp.bitmap << 3) + yOfs];
                        for( unsigned int b = 0; b < 8; ++b ) {
                            unsigned int color = bits & 0b11;
                            if( color | Solid ) {
                                if( x >=0 && x < m_Width )
                                    Buf[x].FastBlend2Src( Pal[color] );
                                ++x;
                                if( x >=0 && x < m_Width )
                                    Buf[x].FastBlend2Src( Pal[color] );
                                ++x;
                            } else
                                x += 2;
                            bits >>= 2;
                        }
                    }
                }
            } else {
                if( Sp.flags.large ) {
                    if( yOfs < 16 ) {
                        int CharOfs = (Sp.bitmap << 3) + yOfs;
                        if( yOfs >= 8 )
                            CharOfs += 8;

                        unsigned int bits = i_CharBitmap[CharOfs];
                        for( unsigned int b = 0; b < 8; ++b ) {
                            unsigned int color = bits & 0b11;
                            if( (color | Solid) && x >=0 && x < m_Width )
                                Buf[x].FastBlend2Src( Pal[color] );
                            bits >>= 2;
                            ++x;
                        }
                        bits = i_CharBitmap[CharOfs + 8];
                        for( unsigned int b = 0; b < 8; ++b ) {
                            unsigned int color = bits & 0b11;
                            if( (color | Solid) && x >=0 && x < m_Width )
                                Buf[x].FastBlend2Src( Pal[color] );
                            bits >>= 2;
                            ++x;
                        }
                    }
                } else {
                    if( yOfs < 8 ) {
                        unsigned short bits = i_CharBitmap[(Sp.bitmap << 3) + yOfs];
                        for( unsigned int b = 0; b < 8; ++b ) {
                            unsigned int color = bits & 0b11;
                            if( (color | Solid) && x >=0 && x < m_Width  )
                                Buf[x].FastBlend2Src( Pal[color] );
                            bits >>= 2;
                            ++x;
                        }
                    }
                }
            }
        }
    }
}
