#ifndef CCHARBITMAP_H_
#define CCHARBITMAP_H_
/*
 * Grafx/CharBitmap/CCharBitmap.h
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Jun 18, 2015
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */


extern "C" {
    #include "Grafx/CharBitmap/CharBitmap.h"
}

extern unsigned short CharBitmap[CharBitmapSize * 8];
extern unsigned short SpriteBitmap[SpriteBitmapSize * 8];


#endif // CCHARBITMAP_H_
