#ifndef PIXEL_HPP_
#define PIXEL_HPP_
/*
 * Grafx/Pixel.hpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Jun 16, 2015
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */


union Pixel {
public:
    unsigned int raw;
    struct {
        unsigned char r;
        unsigned char g;
        unsigned char b;
        unsigned char a;
    } ch;

public:
    Pixel()
    {}

    Pixel( unsigned int i_raw )
    : raw( i_raw )
    {}

    Pixel( unsigned char i_r, unsigned char i_g, unsigned char i_b )
    {
        ch.r = i_r;
        ch.g = i_g;
        ch.b = i_b;
    }

    inline Pixel GetDim() const {
        unsigned int n = raw;
        n >>= 1;
        n &= 0b01111111011111110111111101111111;
        return Pixel( n );
    }

    inline void AddPedestal() {
/*
        ch.r = ((ch.r * 240) >> 8) + 16;
        ch.g = ((ch.g * 240) >> 8) + 16;
        ch.b = ((ch.b * 240) >> 8) + 16;
*/
        raw =   // vector optimized :)
            (  (((raw & 0xff00ff) * 240 + 0x10001000) & 0xff00ff00)
             | (((raw & 0x00ff00) * 240 + 0x00100000) & 0x00ff0000)
            ) >> 8
            | (raw & 0xff000000);
    }

    inline void RemovePedestal() {
/*
        ch.r = ((ch.r - 16) * 274) >> 8;
        ch.g = ((ch.g - 16) * 274) >> 8;
        ch.b = ((ch.b - 16) * 274) >> 8;
*/
        raw =   // vector optimized :)
            (  ((((raw & 0xff00ff) - 0x100010) * 274) & 0xff00ff00)
             | ((((raw & 0x00ff00) - 0x001000) * 274) & 0x00ff0000)
            ) >> 8
            | (raw & 0xff000000);
    }

    // blend using transparency value from source
    void Blend( const Pixel& i_Source ) {
        // vector optimized :)
        const unsigned int T = i_Source.ch.a;

        unsigned long long RGB = (i_Source.GetPartialBlend_64( 255 - T ) + GetPartialBlend_64( T )) & 0xff00ff00ff00;
        raw = (RGB >> 8) | (RGB >> 32);

//        unsigned int RB = (i_Source.GetPartialBlend_RB( 255 - T ) + GetPartialBlend_RB( T )) & 0xff00ff00;
//        unsigned int G = (i_Source.GetPartialBlend_G( 255 - T ) + GetPartialBlend_G( T )) & 0x00ff0000;
//        if( RB & 0x100000000 )    // saturate
//            RB |= 0xff000000;
//        if( RB & 0x000010000 )    // saturate
//            RB |= 0x0000ff00;
//        if( G & 0x001000000 )     // saturate
//            G |= 0x00ff0000;

//        RB &= 0xff00ff00;
//        G  &= 0x00ff0000;

//        if( RB & 0x00010000 || G & 0x01000000 )
//            raw = 255;
//        else
//            raw = (RB | G) >> 8;
//          | (raw & 0xff000000);
    }

    // fast 50/50 blend
    static inline unsigned int FastBlend2( const unsigned int i_Source1, const unsigned int i_Source2 ) {
        // vector optimized :)
        if( i_Source1 & 0x80000000 )
            return i_Source2;
        if( i_Source1 & 0x40000000 )
            return ((i_Source1 >> 1) & 0x007f7f7f) + ((i_Source2 >> 1) & 0x007f7f7f);
        return i_Source1;
    }

    // fast 50/50 conditional blend based on transparency value from source
    inline void FastBlend2Src( const Pixel& i_Source ) {
        // vector optimized :)
        raw = FastBlend2( i_Source.raw, raw );
    }

    // fast 50/50 conditional blend based on transparency value from destination
    inline void FastBlend2Dst( const Pixel& i_Source ) {
        // vector optimized :)
        raw = FastBlend2( raw, i_Source.raw );
    }

private:
    inline unsigned long long GetPartialBlend_64( unsigned int T ) const {
        // vector optimized :)
        return ((raw & 0xff00ff) | ((raw & 0x00ff00LL) << 24)) * T;
    }

    inline unsigned int GetPartialBlend_RB( unsigned int T ) const {
        // vector optimized :)
        return (raw & 0xff00ff) * T;
    }

    inline unsigned int GetPartialBlend_G( unsigned int T ) const {
        // vector optimized :)
        return (raw & 0x00ff00) * T;
    }
};


union PackedPixel {
public:
    unsigned int raw;
    struct {
        unsigned int r:6;
        unsigned int g:6;
        unsigned int b:6;
    } ch;

    PackedPixel( unsigned int i_raw )
    : raw( i_raw )
    {}

    PackedPixel( unsigned char i_r, unsigned char i_g, unsigned char i_b )
    {
        ch.r = i_r;
        ch.g = i_g;
        ch.b = i_b;
    }

    PackedPixel( const Pixel i_Pixel )
    {
        ch.r = i_Pixel.ch.r;
        ch.g = i_Pixel.ch.g;
        ch.b = i_Pixel.ch.b;
    }
};


#endif // PIXEL_HPP_
