#ifndef SCREEN_HPP_
#define SCREEN_HPP_
/*
 * Grafx/Screen.hpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Feb 1, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */


#include "Grafx/LineBuf.hpp"
#include "Grafx/Rect/Canvas.hpp"


class Screen {
public:
    Screen( unsigned int i_Width, unsigned int i_Height, unsigned char i_Fields, unsigned int i_LineBufCount );

    ~Screen();

    void PreRender( unsigned int i_FieldCounter, unsigned char i_Field = 0 );

    void RenderScanLine( unsigned short i_ScanLine, unsigned char i_Field );

    unsigned short OutputScanLine( unsigned short i_ScanLine, unsigned short i_NextVideoClock );

    unsigned int Width() const {
        return m_LineBufSet->Width();
    }

    unsigned int Height() const { return m_Height; }

    Canvas& GetCanvas() { return *m_Canvas; }

    unsigned int CurrentFieldCounter() const { return m_CurrentFieldCounter; }

    unsigned char CurrentField() const { return m_CurrentField; }

    unsigned char Fields() const { return m_Fields; }

    static unsigned int PackPixel( unsigned int i_RawPixel );

private:
    void PackPixels( LineBuf& io_LineBuf );

private:
    unsigned int m_Height;
    unsigned int* m_BufMem;
    LineBufSet* m_LineBufSet;
    Canvas* m_Canvas;
    unsigned int m_CurrentFieldCounter;
    unsigned char m_CurrentField;
    unsigned char m_Fields;
};

#endif /* SCREEN_HPP_ */
