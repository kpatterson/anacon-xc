/*
 * Grafx/Screen.cpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include "Grafx/Screen.hpp"

extern "C" {
    #include "PortIO.h"
}


Screen::Screen( unsigned int i_Width, unsigned int i_Height, unsigned char i_Fields, unsigned int i_LineBufCount )
: m_Height( i_Height )
, m_CurrentFieldCounter( 0 )
, m_CurrentField( 0 )
, m_Fields( i_Fields )
{
    if( m_Fields < 1 )
        m_Fields = 1;

    m_BufMem = new unsigned int[i_Width * i_LineBufCount];
    m_LineBufSet = new LineBufSet( m_BufMem, i_Width, i_LineBufCount );
    m_Canvas = new Canvas( 0, 0, i_Width, i_Height );
}

Screen::~Screen() {
    delete m_Canvas;
    delete m_LineBufSet;
    delete[] m_BufMem;
}

//TODO: how many stack words do we need? it depends...
#pragma stackfunction 64
void Screen::PreRender( unsigned int i_FieldCounter, unsigned char i_Field ) {
    m_CurrentFieldCounter = i_FieldCounter;

    if( i_Field >= m_Fields )
        i_Field = m_Fields - 1;

    m_CurrentField = i_Field;

    m_Canvas->PreRender( *this );
}

//TODO: how many stack words do we need? it depends...
#pragma stackfunction 64
void Screen::RenderScanLine( unsigned short i_ScanLine, unsigned char i_Field ) {
    if( i_ScanLine >= m_Height )
        return;

    LineBuf t_LineBuf( *m_LineBufSet, i_ScanLine );

    if( i_Field >= m_Fields )
        i_Field = m_Fields - 1;

    m_Canvas->Render( (i_ScanLine * m_Fields) + i_Field, t_LineBuf );

    PackPixels( t_LineBuf );
}

unsigned short Screen::OutputScanLine( unsigned short i_ScanLine, unsigned short i_NextVideoClock ) {
    if( i_ScanLine >= m_Height )
        return i_NextVideoClock;

    LineBuf t_LineBuf( *m_LineBufSet, i_ScanLine );
    return XCOutputScanLinePtr( t_LineBuf.GetMem(), t_LineBuf.Width(), i_NextVideoClock );
}

void Screen::PackPixels( LineBuf& io_LineBuf ) {
    unsigned int* const t_Buf = io_LineBuf.GetMem();
    const unsigned int end = io_LineBuf.Width();
    for( unsigned int i = 0; i < end; ++i ) {
        unsigned int x = t_Buf[i] >> 2;
        t_Buf[i] =  (x & 0x0000003f)
               | ((x & 0x003f0000) >> 4)
               | ((x & 0x00003f00) >> 2);
    }
}

unsigned int Screen::PackPixel( unsigned int i_RawPixel ) {
    unsigned int x = i_RawPixel >> 2;
    i_RawPixel =  (x & 0x0000003f)
           | ((x & 0x003f0000) >> 4)
           | ((x & 0x00003f00) >> 2);
    return i_RawPixel;
}
