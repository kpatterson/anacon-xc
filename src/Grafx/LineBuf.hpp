#ifndef LINEBUF_HPP_
#define LINEBUF_HPP_
/*
 * Grafx/LineBuf.hpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Feb 1, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include "Grafx/Pixel.hpp"


class LineBufSet {
public:
    LineBufSet( unsigned int* i_Mem, unsigned int i_Width, unsigned int i_BufCount )
    : m_Mem( i_Mem )
    , m_Width( i_Width )
    , m_BufCount( i_BufCount )
    {}

    unsigned int* GetMem( unsigned int i_BufNum ) {
        return m_Mem + m_Width * i_BufNum;
    }

    Pixel* GetPixels( unsigned int i_BufNum ) {
        return reinterpret_cast<Pixel*>( m_Mem + m_Width * i_BufNum );
    }

    unsigned int Width() const {
        return m_Width;
    }

    unsigned int BufCount() const {
        return m_BufCount;
    }

private:
    unsigned int* m_Mem;
    unsigned int m_Width;
    unsigned int m_BufCount;
};


class LineBuf {
public:
    LineBuf( LineBufSet& i_LineBufSet, unsigned int i_BufNum )
    : m_LineBufSet( i_LineBufSet )
    , m_BufNum( i_BufNum % i_LineBufSet.BufCount() )
    {}

    unsigned int* GetMem() {
        return m_LineBufSet.GetMem( m_BufNum );
    }

    Pixel* GetPixels() {
        return m_LineBufSet.GetPixels( m_BufNum );
    }

    unsigned int Width() const {
        return m_LineBufSet.Width();
    }

private:
    LineBufSet& m_LineBufSet;
    unsigned int m_BufNum;
};

#endif /* LINEBUF_HPP_ */
