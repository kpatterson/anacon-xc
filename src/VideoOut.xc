/*
 * VideoOut.xc
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: May 7, 2015
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */


extern "C"  {
    #include "Globals.h"
    #include "CppMain.h"
    #include "Grafx/Palette/Palette.h"
}

#include "PortIO.xc.h"
#include "VideoOut.xc.h"
#include "UserInput/PS2/PS2.xc.h"


out buffered port:32 VideoDAC = XS1_PORT_32A;
out buffered port:1 DACClock = XS1_PORT_1I;
out buffered port:1 VideoHSync = XS1_PORT_1O;
out buffered port:1 VideoVSync = XS1_PORT_1P;


//timer VideoClock;


unsigned short Lines;
unsigned char Fields;
unsigned short FieldLines;
//float HFreq;

unsigned short LineClocks;

unsigned short HSyncClocks;
unsigned short VSyncClocks;
unsigned short VSyncSerrClocks;
unsigned short VSyncEquClocks;

unsigned short ColorBurstClocks;
unsigned short BackPorchAClocks;
unsigned short BackPorchBClocks;

unsigned short FrontPorchClocks;

unsigned short ActiveFirstLine;
unsigned short ActiveLines;

unsigned char DominantField;

unsigned short ActiveHOffset;


unsigned short NextVideoClock;
unsigned short LineStartVideoClock;


unsigned int FieldCounter;


void PartOutputLineStart() {
    LineStartVideoClock = NextVideoClock;

    // H Sync
    VideoHSync @ NextVideoClock <: 0;
    NextVideoClock += HSyncClocks;
    VideoHSync @ NextVideoClock <: 1;

    // Back Porch, before Colorburst
    XCOutputColor( Blank, NextVideoClock );
    NextVideoClock += BackPorchAClocks;

    // Colorburst
    XCOutputColor( ColorBurst, NextVideoClock );
    NextVideoClock += ColorBurstClocks;

    // Back Porch, after Colorburst
    XCOutputColor( Blank, NextVideoClock );
    NextVideoClock += BackPorchBClocks;
}

void PartOutputLineEnd() {
    // Front Porch
    XCOutputColor( Blank, NextVideoClock );
    NextVideoClock = LineStartVideoClock + LineClocks;
}

void PartOutputHalfLineEnd() {
    // Front Porch
    XCOutputColor( Blank, NextVideoClock );
    NextVideoClock = LineStartVideoClock + (LineClocks >> 1);
}

void OutputLineBorder() {
    PartOutputLineStart();

    XCOutputColor( Border, NextVideoClock );

    NextVideoClock = LineStartVideoClock + LineClocks - FrontPorchClocks;
    PartOutputLineEnd();
}

void OutputHalfLineBorder() {
    PartOutputLineStart();

    XCOutputColor( Border, NextVideoClock );

    NextVideoClock = LineStartVideoClock + (LineClocks >> 1) - FrontPorchClocks;
    PartOutputHalfLineEnd();
}

void OutputLineVBlank() {
    LineStartVideoClock = NextVideoClock;

    // Equalization Pulse on HSync
    VideoHSync @ NextVideoClock <: 0;
    NextVideoClock += VSyncEquClocks;
    VideoHSync @ NextVideoClock <: 1;

    XCOutputColor( Blank, NextVideoClock );

    NextVideoClock = LineStartVideoClock + (LineClocks >> 1);

    // Equalization Pulse on HSync
    VideoHSync @ NextVideoClock <: 0;
    NextVideoClock += VSyncEquClocks;
    VideoHSync @ NextVideoClock <: 1;

    NextVideoClock = LineStartVideoClock + LineClocks;
}

void OutputLineVSync() {
    LineStartVideoClock = NextVideoClock;

    // VSync On
    VideoVSync @ NextVideoClock <: 0;

    NextVideoClock += (LineClocks >> 1) - VSyncSerrClocks;

    // Serration Pulse on HSync
    VideoHSync @ NextVideoClock <: 0;
    NextVideoClock += VSyncSerrClocks;
    VideoHSync @ NextVideoClock <: 1;

    // VSync Off
    NextVideoClock = LineStartVideoClock + VSyncClocks;
    VideoVSync @ NextVideoClock <: 1;

    NextVideoClock = LineStartVideoClock + LineClocks;
}

void OutputLineActive( unsigned short i_ScanLine ) {
    PartOutputLineStart();

    // left border
    XCOutputColor( Border, NextVideoClock );
    NextVideoClock += ActiveHOffset + 14;

    // active
    /* NextVideoClock = */ OutputScanLine( i_ScanLine, NextVideoClock );

    // right border
//    XCOutputColor( Border, NextVideoClock );

    NextVideoClock = LineStartVideoClock + LineClocks - FrontPorchClocks;
    PartOutputLineEnd();
}

void RenderLine( unsigned short i_ScanLine, unsigned char i_Field ) {
    if( i_ScanLine >= ActiveLines )
        return;

    RenderScanLine( i_ScanLine, i_Field );
}

void ProcessPS2Events( chanend i_PS2Chan ) {
    // Process PS2 Events
    unsigned char t_ChanCommand;

#ifdef EnablePS2Log
    // process PS2 Debug Log
    {
        t_ChanCommand = 1;
        i_PS2Chan <: t_ChanCommand;

        unsigned char t_Header;     // should be 1
        i_PS2Chan :> t_Header;

        unsigned int t_LogSize;
        i_PS2Chan :> t_LogSize;

        char t_Log[256];
        unsigned int t_LogMax = 256;
        if( t_LogSize < t_LogMax )
            t_LogMax = t_LogSize;

        unsigned int i = 0;
        for( ; i < t_LogMax; ++i )
             i_PS2Chan :> t_Log[i];

        for( ; i < t_LogSize; ++i )
             i_PS2Chan :> char;

        i_PS2Chan :> t_Header;      // should be 0

        AppendLog( t_Log, t_LogSize );
    }
#endif // EnablePS2Log

    // process PS2 mouse events
    {
        t_ChanCommand = 2;
        i_PS2Chan <: t_ChanCommand;

        unsigned char t_Header;
        i_PS2Chan :> t_Header;      // should be 1

        unsigned int t_Size;
        i_PS2Chan :> t_Size;

        MouseEvent_t t_MouseEvent[16];
        unsigned int t_Max = 16;
        if( t_Size < t_Max )
            t_Max = t_Size;

        unsigned int i = 0;
        for( ; i < t_Max; ++i )
             i_PS2Chan :> t_MouseEvent[i];

        for( ; i < t_Size; ++i )
             i_PS2Chan :> MouseEvent_t;

        i_PS2Chan :> t_Header;      // should be 0

        OnMouseEvents( t_MouseEvent, t_Max );
    }

    // process PS2 keyboard events
    {
        t_ChanCommand = 3;
        i_PS2Chan <: t_ChanCommand;

        unsigned char t_Header;
        i_PS2Chan :> t_Header;      // should be 1

        unsigned int t_Size;
        i_PS2Chan :> t_Size;

        KeybEvent_t t_KeybEvent[16];
        unsigned int t_Max = 16;
        if( t_Size < t_Max )
            t_Max = t_Size;

        unsigned int i = 0;
        for( ; i < t_Max; ++i )
             i_PS2Chan :> t_KeybEvent[i];

        for( ; i < t_Size; ++i )
             i_PS2Chan :> KeybEvent_t;

        i_PS2Chan :> t_Header;      // should be 0

        OnKeybEvents( t_KeybEvent, t_Max );
    }
}

void VideoOutLoop( chanend i_PS2Chan ) {
    NextVideoClock = 0;
    FieldCounter = 0;

    while( 1 ) {
        unsigned short ScanLine = 0;

        par {
            {
                set_core_high_priority_on();
                // NTSC Vertical Blanking Interval
                {
                    while( ScanLine < 3 ) {
                        OutputLineVBlank();
                        ScanLine++;
                    }

                    while( ScanLine < 6 ) {
                        OutputLineVSync();
                        ScanLine++;
                    }

                    while( ScanLine < 9 ) {
                        OutputLineVBlank();
                        ScanLine++;
                    }
                }

                // Top Border
                while( ScanLine < ActiveFirstLine ) {
                    OutputLineBorder();
                    ScanLine++;
                }
//                set_core_high_priority_off();
            }
            {
                ProcessPS2Events( i_PS2Chan );

                // Active Image, Field 0
                VBIHook( ++FieldCounter, DominantField );

                for( unsigned int ActiveLine = 0; ActiveLine < (ScanLineBufferCount >> 1); ++ActiveLine )
                    RenderLine( ActiveLine, DominantField );
            }
        }

        {
            unsigned short ActiveLine = 0;
            while( (ActiveLine < ActiveLines) && (ScanLine < FieldLines) ) {
                par {
                    {
                        set_core_high_priority_on();
                        for( unsigned int i = 0; i < (ScanLineBufferCount >> 1); ++i )
                            OutputLineActive( ActiveLine + i );

//                        set_core_high_priority_off();
                    }
    //                RenderLine( ActiveLine + 1, DominantField );
                    RenderLine( ActiveLine + 2, DominantField );
                    RenderLine( ActiveLine + 3, DominantField );
    //                RenderLine( ActiveLine + 4, DominantField );
    //                RenderLine( ActiveLine + 5, DominantField );
    //                RenderLine( ActiveLine + 6, DominantField );
    //                RenderLine( ActiveLine + 7, DominantField );
                }
                ActiveLine += (ScanLineBufferCount >> 1);
                ScanLine += (ScanLineBufferCount >> 1);
            }
        }

        par {
            {
                set_core_high_priority_on();
                // Bottom Border
                while( ScanLine < FieldLines ) {
                    OutputLineBorder();
                    ScanLine++;
                }

                // Half Line
                OutputHalfLineBorder();

                ScanLine = 0;

                // NTSC Vertical Blanking Interval
                {
                    while( ScanLine < 3 ) {
                        OutputLineVBlank();
                        ScanLine++;
                    }

                    while( ScanLine < 6 ) {
                        OutputLineVSync();
                        ScanLine++;
                    }

                    while( ScanLine < 9 ) {
                        OutputLineVBlank();
                        ScanLine++;
                    }
                }

                // Half Line
                OutputHalfLineBorder();

                // Top Border
                while( ScanLine < ActiveFirstLine ) {
                    OutputLineBorder();
                    ScanLine++;
                }
//                set_core_high_priority_off();
            }
            {
                ProcessPS2Events( i_PS2Chan );

                // Active Image, Field 1
                VBIHook( ++FieldCounter, DominantField ^ 1 );

                for( unsigned int ActiveLine = 0; ActiveLine < (ScanLineBufferCount >> 1); ++ActiveLine )
                    RenderLine( ActiveLine, DominantField ^ 1 );
            }
        }

        {
            unsigned short ActiveLine = 0;
            while( (ActiveLine < ActiveLines) && (ScanLine < FieldLines) ) {
                par {
                    {
                        set_core_high_priority_on();
                        for( unsigned int i = 0; i < (ScanLineBufferCount >> 1); ++i )
                            OutputLineActive( ActiveLine + i );

//                        set_core_high_priority_off();
                    }
    //                RenderLine( ActiveLine + 1, DominantField ^ 1 );
                    RenderLine( ActiveLine + 2, DominantField ^ 1 );
                    RenderLine( ActiveLine + 3, DominantField ^ 1 );
    //                RenderLine( ActiveLine + 4, DominantField ^ 1 );
    //                RenderLine( ActiveLine + 5, DominantField ^ 1 );
    //                RenderLine( ActiveLine + 6, DominantField ^ 1 );
    //                RenderLine( ActiveLine + 7, DominantField ^ 1 );
                }
                ActiveLine += (ScanLineBufferCount >> 1);
                ScanLine += (ScanLineBufferCount >> 1);
            }
        }

        // Bottom Border
        while( ScanLine < FieldLines ) {
            OutputLineBorder();
            ScanLine++;
        }
    }
}

void VideoOutInit() {
    Lines = 525;
    Fields = 2;
    FieldLines = Lines / Fields;
    //HFreq = Lines * 30000.0 / 1001.0;                 // HFreq = 15734 Hz

    LineClocks = (100000000 / 15734) + 0;             // 6356; H = 63.56 �s

    HSyncClocks = 470;                                // HSync pulses are 4.7 �s
    VSyncSerrClocks = LineClocks * 0.07;              // VSync Serration pulses are 7% H, repeated at 2HFreq
    VSyncEquClocks = LineClocks * 0.04;               // VSync Equalization pulses are 4% H, repeated at 2HFreq
    VSyncClocks = LineClocks - VSyncSerrClocks;

    ColorBurstClocks = 250;     // Color Burst is 2.5 �s, centered on Back Porch
    BackPorchAClocks = 110;     // Back Porch is 4.7 �s
    BackPorchBClocks = 110;

    FrontPorchClocks = 150;     // Front Porch is 1.5 �s

    ActiveFirstLine = 21;       // min = 20; normal = 21;
    ActiveLines = 240;

    DominantField = 0;

    ActiveHOffset = 220;
}
