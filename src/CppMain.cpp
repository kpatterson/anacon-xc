/*
 * CppMain.cpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Feb 1, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

extern "C" {
    #include "Globals.h"
    #include "CppMain.h"
    #include "UserInput/MouseEvent.h"
    #include "UserInput/KeybEvent.h"
}


#include "Grafx/Screen.hpp"
#include "Grafx/Rect/Box.hpp"
#include "Grafx/Rect/Circle.hpp"
#include "Grafx/Rect/TextBox.hpp"
#include "Grafx/Rect/SpriteArea.hpp"

#include "Grafx/Palette/Palette.hpp"


Screen* Screen0 = 0;
Box* Box0 = 0;
TextBox* TextBox0 = 0;
SpriteArea* SpriteArea0 = 0;
Circle* Circle0 = 0;

const unsigned short VOffset = 0; //11

char VLog[1024];
unsigned int VLogSize = 0;

extern "C" {

    void CppInit() {
        Screen0 = new Screen( 320, 240, 1, ScanLineBufferCount );

        TextBox0 = new TextBox( 0, 0, 40, 30 );
        Screen0->GetCanvas().GetRects().push_back( TextBox0 );

        // background box
        Box0 = new Box( 160, 120, 40, 40, GetColorAsRawPixel( 7 ) );
        Screen0->GetCanvas().GetRects().push_back( Box0 );

        TextBox0->DisplayString( 0, 2, "\02 Kevin H. Patterson \02" );
        TextBox0->DisplayString( 2, 4, "\02 Shawna Patterson \02" );
        TextBox0->DisplayString( 1, 6, "\02 Abigail Patterson \02" );
        TextBox0->DisplayString( 2, 8, "\02 Daniel Patterson \02" );

        TextBox0->DisplayString( 1, 10, "ABCDEFGHIJKLMNOPQRSTUVWXYZnin" );
        TextBox0->DisplayString( 1, 11, "abcde|ghijklmnopqrstuvwxyznxn" );
        TextBox0->DisplayString( 1, 12, "/~0:1;2,3.456789!@#$%^&*+\xda-_=" );
        TextBox0->DisplayString( 1, 13, "\\f?;(8).[8]{8}<8>`8'8'\"8\"\xfbK's" );

        TextBox0->DisplayString( 0, -1, "Last Line! :)" );

        SpriteArea0 = new SpriteArea( 0, 0, 320, 240, 8, true );
        Screen0->GetCanvas().GetRects().push_back( SpriteArea0 );

        Circle0 = new Circle( 160, 120, 38, 40, GetColorAsRawPixel( 1 ) );
        Screen0->GetCanvas().GetRects().push_back( Circle0 );
    }

    void RenderScanLine( unsigned short i_ScanLine, unsigned char i_Field ) {
        Screen0->RenderScanLine( i_ScanLine - VOffset, i_Field );
    }

    unsigned short OutputScanLine( unsigned short i_ScanLine, unsigned short i_NextVideoClock ) {
        return Screen0->OutputScanLine( i_ScanLine - VOffset, i_NextVideoClock );
        return i_NextVideoClock;
    }

    void VBIHook( unsigned int i_FieldCounter, unsigned char i_Field ) {
        TextBox0->DisplayInt( -1, 0, i_FieldCounter );
        Screen0->PreRender( i_FieldCounter, i_Field );
    }

    void AppendLog( char* i_Text, unsigned int i_Size ) {
        for( unsigned int i = 0; i < i_Size; ++i )
            VLog[VLogSize++] = i_Text[i];

        VLog[VLogSize] = 0;

        TextBox0->DisplayString( 0, 15, VLog );
    }

    void OnMouseEvents( MouseEvent_t* i_MouseEvents, unsigned int i_Size ) {
        static short MouseX = 0;
        static short MouseY = 0;

        bool t_HasMotion = false;
        for( unsigned int i = 0; i < i_Size; ++i ) {
            if( i_MouseEvents[i].Code == Motion ) {
                MouseX += i_MouseEvents[i].X;
                MouseY += i_MouseEvents[i].Y;
                t_HasMotion = true;
            }
        }

        if( t_HasMotion ) {
            if( MouseX < 0 ) MouseX = 0;
            if( MouseY < 0 ) MouseY = 0;
            if( MouseX > 640 ) MouseX = 640;
            if( MouseY > 480 ) MouseY = 480;

            TextBox0->DisplayStringR( -2, 3, "    " );
            TextBox0->DisplayStringR( -2, 4, "    " );
            TextBox0->DisplayInt( -1, 3, MouseX );
            TextBox0->DisplayInt( -1, 4, MouseY );

            SpriteArea0->SetSpriteCoords( 0, MouseX, MouseY );
        }
    }

    void DisplayBits( short i_Col, short i_Row, unsigned char i_Byte ) {
        char t_Bits[9];
        for( int i = 0; i < 8; ++i ) {
            t_Bits[i] = (i_Byte & 0x01) ? '1' : '0';
            i_Byte >>= 1;
        }
        t_Bits[8] = 0;
        TextBox0->DisplayStringR( i_Col, i_Row, t_Bits );
    }

    void OnKeybEvents( KeybEvent_t* i_KeybEvents, unsigned int i_Size ) {
        for( unsigned int i = 0; i < i_Size; ++i ) {
            if( i_KeybEvents[i].Char )
                VLog[VLogSize++] = i_KeybEvents[i].Char;
        }
        VLog[VLogSize] = 0;

        if( i_Size ) {
            DisplayBits( -1, 6, i_KeybEvents[i_Size - 1].Flags );
            DisplayBits( -1, 7, i_KeybEvents[i_Size - 1].Mods );
        }

        TextBox0->DisplayString( 0, 15, VLog );
    }
}
