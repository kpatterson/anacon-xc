/*
 * UserInput/KeybTranslate.cpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Feb 11, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include "UserInput/KeybTranslate.hpp"

#define TableSize 128

char TableLowerASCII[TableSize] = {
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x09,  '`',    0,
    0,    0,    0,    0,    0,  'q',  '1',    0,    0,    0,  'z',  's',  'a',  'w',  '2',    0,
    0,  'c',  'x',  'd',  'e',  '4',  '3',    0,    0,  ' ',  'v',  'f',  't',  'r',  '5',    0,
    0,  'n',  'b',  'h',  'g',  'y',  '6',    0,    0,    0,  'm',  'j',  'u',  '7',  '8',    0,
    0,  ',',  'k',  'i',  'o',  '0',  '9',    0,    0,  '.',  '/',  'l',  ';',  'p',  '-',    0,
    0,    0, '\'',    0,  '[',  '=',    0,    0,    0,    0, 0x0A,  ']',    0, '\\',    0,    0,
    0,    0,    0,    0,    0,    0, 0x08,    0,    0,  '1',    0,  '4',  '7',    0,    0,    0,
  '0',  '.',  '2',  '5',  '6',  '8', 0x1B,    0,    0,  '+',  '3',  '-',  '*',  '9',    0,    0
};

char TableUpperASCII[TableSize] = {
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x09,  '~',    0,
    0,    0,    0,    0,    0,  'Q',  '!',    0,    0,    0,  'Z',  'S',  'A',  'W',  '@',    0,
    0,  'C',  'X',  'D',  'E',  '$',  '#',    0,    0,  ' ',  'V',  'F',  'T',  'R',  '%',    0,
    0,  'N',  'B',  'H',  'G',  'Y',  '^',    0,    0,    0,  'M',  'J',  'U',  '&',  '*',    0,
    0,  '<',  'K',  'I',  'O',  ')',  '(',    0,    0,  '>',  '?',  'L',  ':',  'P',  '_',    0,
    0,    0,  '"',    0,  '{',  '+',    0,    0,    0,    0, 0x0A,  '}',    0,  '|',    0,    0,
    0,    0,    0,    0,    0,    0, 0x08,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0, 0x1B,    0,    0,  '+',    0,    0,  '*',    0,    0,    0
};

extern "C" {

    void TranslateScanCode( KeybEvent_t& i_KeybEvent ) {

        if( i_KeybEvent.Flags & KeyBreak ) {
            if( i_KeybEvent.Flags & ExtE0 ) {
                switch( i_KeybEvent.Scan ) {
                    case 0x14:  // R Ctrl
                        i_KeybEvent.Mods &= ~RightCtrl; break;
                    case 0x11:  // R Alt
                        i_KeybEvent.Mods &= ~RightAlt; break;
                    case 0x1F:  // L Meta
                        i_KeybEvent.Mods &= ~LeftMeta; break;
                    case 0x27:  // R Meta
                        i_KeybEvent.Mods &= ~RightMeta; break;
                }
            } else {
                switch( i_KeybEvent.Scan ) {
                    case 0x12:  // L Shift
                        i_KeybEvent.Mods &= ~LeftShift; break;
                    case 0x59:  // R Shift
                        i_KeybEvent.Mods &= ~RightShift; break;
                    case 0x14:  // L Ctrl
                        i_KeybEvent.Mods &= ~LeftCtrl; break;
                    case 0x11:  // L Alt
                        i_KeybEvent.Mods &= ~LeftAlt; break;
                }
            }
            return;

        } else {

            if( i_KeybEvent.Flags & ExtE0 ) {
                switch( i_KeybEvent.Scan ) {
                    case 0x14:  // R Ctrl
                        i_KeybEvent.Mods |= RightCtrl; break;
                    case 0x11:  // R Alt
                        i_KeybEvent.Mods |= RightAlt; break;
                    case 0x1F:  // L Meta
                        i_KeybEvent.Mods |= LeftMeta; break;
                    case 0x27:  // R Meta
                        i_KeybEvent.Mods |= RightMeta; break;

                    case 0x70:  // Insert
                        i_KeybEvent.Flags ^= InsertMode; break;
                }
            } else {
                switch( i_KeybEvent.Scan ) {
                    case 0x12:  // L Shift
                        i_KeybEvent.Mods |= LeftShift; break;
                    case 0x59:  // R Shift
                        i_KeybEvent.Mods |= RightShift; break;
                    case 0x14:  // L Ctrl
                        i_KeybEvent.Mods |= LeftCtrl; break;
                    case 0x11:  // L Alt
                        i_KeybEvent.Mods |= LeftAlt; break;


                    case 0x7E:  // Scroll Lock
                        i_KeybEvent.Flags ^= ScrollLock; break;

                    case 0x77:  // Num Lock
                        i_KeybEvent.Flags ^= NumLock; break;

                    case 0x58:  // Caps Lock
                        i_KeybEvent.Flags ^= CapsLock; break;
                }
            }
        }

        if( (i_KeybEvent.Flags & ExtE0) && (i_KeybEvent.Scan != 0x4A) )
            return;

        if( i_KeybEvent.Scan < 0x01 || i_KeybEvent.Scan > 0x7F || (i_KeybEvent.Mods & (Ctrl | Alt | Meta)) )
            return;

        char t_Char = TableLowerASCII[i_KeybEvent.Scan];

        bool t_Shift = i_KeybEvent.Mods & Shift;
        if( t_Char >= 'a' && t_Char <= 'z' )
            t_Shift ^= bool( i_KeybEvent.Flags & CapsLock );

        if( t_Shift )
            t_Char = TableUpperASCII[i_KeybEvent.Scan];

        i_KeybEvent.Char = t_Char;
    }

}
