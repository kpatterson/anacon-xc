/*
 * UserInput/PS2/PS2.xc
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Feb 7, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

extern "C" {
    #include "Globals.h"
    #include "CppMain.h"
    #include "UserInput/PS2/PS2Framer.h"
    #include "UserInput/KeybTranslate.h"
}

#include "UserInput/PS2/PS2.xc.h"


#ifndef PS2DeviceCount
    #define PS2DeviceCount 1
#endif

#ifndef PS2_INV
    #define PS2_INV 0
#endif

#ifndef PS2_LO
    #define PS2_LO 0
#endif

#ifndef PS2_HI
    #define PS2_HI 1
#endif


//ports

typedef struct PS2Port_t {
    in buffered port:1 ClkIn;
    in buffered port:1 DataIn;
    out buffered port:1 ClkOut;
    out buffered port:1 DataOut;
    unsigned short NextClock;
} PS2Port_t;

PS2Port_t PS2Port[PS2DeviceCount] = { { XS1_PORT_1F, XS1_PORT_1H, XS1_PORT_1G, XS1_PORT_1E }, { XS1_PORT_1K, XS1_PORT_1A, XS1_PORT_1M, XS1_PORT_1N } };

#ifdef EnableScopeTrig
    out buffered port:1 ScopeTrigOut = XS1_PORT_1J;
#endif // EnableScopeTrig

// timers

timer PS2Timer;


// globals

//#define EnablePS2Log
//#define EnableScopeTrig


#ifdef EnablePS2Log

    #ifndef PS2LogMax
        #define PS2LogMax 256
    #endif

    char Log[PS2LogMax];
    unsigned int LogIdx = 0;

#endif // EnablePS2Log


#define StdSleep   050000000  // 0.5 sec
#define StdTimeout 100000000  // 2.0 sec

#define TimeoutEvent 0xFFFF


#ifndef MouseEventMax
    #define MouseEventMax 16
#endif

MouseEvent_t MouseEvent[MouseEventMax];
unsigned char MouseEventIdx = 0;


#ifndef KeybEventMax
    #define KeybEventMax 16
#endif

KeybEvent_t KeybEvent[KeybEventMax];
unsigned char KeybEventIdx = 0;


typedef struct PS2State_t {
    unsigned int Wait;
    unsigned int Timeout;
    unsigned char DeviceIndex;
    unsigned char StateID;
    unsigned char ClkLevel;
    unsigned char ClkLo;
    unsigned char Aux0;
    unsigned char Aux1;
    unsigned char Aux2;
    unsigned char Aux3;
} PS2State_t;

PS2State_t PS2State[PS2DeviceCount];


#ifdef EnableScopeTrig
void ScopeTrig( unsigned int i_Timestamp ) {
    ScopeTrigOut @ i_Timestamp <: 1;
    ScopeTrigOut @ i_Timestamp + 100 <: 0;
}
#endif // EnableScopeTrig

unsigned short ByteToHexStr( unsigned char i_Byte ) {
    static unsigned char Ary[16] = { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F' };
    char L = Ary[i_Byte & 0x0F];
    char H = Ary[(i_Byte >> 4)];
    return L | (H << 8);
}

int WaitForClock( PS2Port_t& io_Port, unsigned int i_Timeout ) {
    int tt;
    PS2Timer :> tt;
    select {
        case io_Port.ClkIn when pinseq( PS2_HI ) :> void:
            break;
        case PS2Timer when timerafter( tt + i_Timeout ) :> void:
            return 1;
            break;
    }
    PS2Timer :> tt;
    select {
        case io_Port.ClkIn when pinseq( PS2_LO ) :> void @ io_Port.NextClock:
            return 0;
            break;
        case PS2Timer when timerafter( tt + i_Timeout ) :> void:
            return 1;
            break;
    }
    return 1;
}

void SendStart( PS2Port_t& io_Port ) {
    //TODO: We should inhibit data from all devices by holding Clk Lo during data send operations

    io_Port.DataIn :> void @ io_Port.NextClock;       // Get current port timestamp

#ifdef EnableScopeTrig
    io_Port.NextClock += 100;
    ScopeTrig( io_Port.NextClock );
    io_Port.NextClock += 500;
#endif // EnableScopeTrig

    io_Port.DataOut <: PS2_HI;                        // Release Data
    io_Port.NextClock += 1000;
    io_Port.ClkOut @ io_Port.NextClock <: PS2_LO;     // Drive Clk Lo
    io_Port.NextClock += 15000;
    io_Port.DataOut @ io_Port.NextClock <: PS2_LO;    // Drive Data Lo
    io_Port.NextClock += 1000;
    io_Port.ClkOut @ io_Port.NextClock <: PS2_HI;     // Release Clk
    io_Port.NextClock += 500;
}

int SendBit( PS2Port_t& io_Port, int i_Bit ) {
    if( WaitForClock( io_Port, 25000 ) )
        return 1;

    io_Port.NextClock += 500;
    io_Port.DataOut @ io_Port.NextClock <: i_Bit ^ PS2_INV;  // Send Data Bit
    return 0;
}

int ReceiveBit( PS2Port_t& io_Port, int& o_Bit, unsigned int i_Timeout ) {
    if( WaitForClock( io_Port, i_Timeout ) )
        return 1;

    io_Port.NextClock += 2000;      // wait 20 �s after falling clock edge before reading data (middle of bit)
    io_Port.DataIn @ io_Port.NextClock :> o_Bit;
    o_Bit = (o_Bit & 0x01) ^ PS2_INV;
    return 0;
}

int SendByte( PS2Port_t& io_Port, unsigned char i_Byte ) {
    SendStart( io_Port );

    int Parity = 1;                      // odd parity
    for( unsigned int i = 0; i < 8; ++i ) {
        int Data = i_Byte & 0x01;
        i_Byte >>= 1;
        Parity ^= Data;

        if( SendBit( io_Port, Data ) )   // Send Data Bit
            return 1;
    }
    if( SendBit( io_Port, Parity ) )     // Send Parity Bit
        return 1;

    if( SendBit( io_Port, 1 ) )          // Send Stop Bit
        return 1;

    int Ack = 0;
    if( ReceiveBit( io_Port, Ack, 25000 ) )     // Receive Ack Bit
        return 1;

    if( Ack == 0 )
        return 0;

    return 1;
}

#ifdef EnablePS2Log
void LogByte( char i_Type, unsigned char i_Byte ) {
    unsigned short C2 = ByteToHexStr( i_Byte );
    Log[LogIdx++] = i_Type;
    Log[LogIdx++] = (C2 >> 8);
    Log[LogIdx++] = C2 & 0xFF;
}

int LogSendByte( PS2State_t& io_Device, unsigned char i_Byte ) {
    int Err = SendByte( PS2Port[io_Device.DeviceIndex], i_Byte );

    int t_PS2LogMax = PS2LogMax - 4;
    if( Err ) t_PS2LogMax -= 4;

    if( LogIdx < t_PS2LogMax ) {
        LogByte( 'P' + io_Device.DeviceIndex, i_Byte );
        if( Err ) {
            Log[LogIdx++] = ':';
            LogByte( 'e', Err );
        }

        Log[LogIdx++] = ',';
    }

    return Err;
}

int LogReceiveByte( PS2State_t& io_Device, unsigned short i_Byte ) {
    unsigned char Err = i_Byte >> 8;

    int t_PS2LogMax = PS2LogMax - 4;
    if( Err ) t_PS2LogMax -= 4;

    if( LogIdx < t_PS2LogMax ) {
        LogByte( 'p' + io_Device.DeviceIndex, i_Byte & 0xFF );
        if( Err ) {
            Log[LogIdx++] = ':';
            LogByte( 'e', Err );
        }

        Log[LogIdx++] = ',';
    }

    return Err;
}
#endif // EnablePS2Log

void DeviceSendByte( PS2State_t& io_Device, unsigned char i_Byte ) {
#ifdef EnablePS2Log
    LogSendByte( io_Device, i_Byte );
#else // EnablePS2Log
    SendByte( PS2Port[io_Device.DeviceIndex], i_Byte );
#endif // EnablePS2Log
}

void SetTimeout( PS2State_t& io_Device, unsigned int i_Delay ) {
    unsigned int t_Timestamp;
    PS2Timer :> t_Timestamp;
    t_Timestamp += i_Delay;
    if( !t_Timestamp )
        ++t_Timestamp;

    io_Device.Timeout = t_Timestamp;
}

void DeviceReset( PS2State_t& io_Device ) {
    PS2Framer_Reset( io_Device.DeviceIndex );
    io_Device.StateID = 0;
    DeviceSendByte( io_Device, 0xFF );
    io_Device.Wait = StdSleep;
    SetTimeout( io_Device, StdTimeout );
    io_Device.ClkLevel = PS2_HI;
    io_Device.ClkLo = 0;
    io_Device.Aux0 = 0;
    io_Device.Aux1 = 0;
    io_Device.Aux2 = 0;
    io_Device.Aux3 = 0;
}

void OnDeviceEvent( PS2State_t& io_Device, unsigned short i_Byte ) {
#ifdef EnablePS2Log
    if( i_Byte != TimeoutEvent )
        LogReceiveByte( io_Device, i_Byte );
#endif // EnablePS2Log

    switch( io_Device.StateID ) {
        case 0:                                          // initial state after reset
            if( i_Byte == 0xFA ) break;                  // skip ack
            if( i_Byte == 0xAA ) {                       // self-test passed
                ++io_Device.StateID;
                break;
            }
            DeviceReset( io_Device ); break;             // else reset

        // auto-detect PS/2 device

        case 1:
            if( i_Byte == 0x00 ) {                       // mouse id: 0
                ++io_Device.StateID;
                DeviceSendByte( io_Device, 0xE8 );       // Set Resolution
                break;
            }
            if( i_Byte == TimeoutEvent ) {
                DeviceSendByte( io_Device, 0xED );       // test to see if this is a keyboard...
                io_Device.StateID = 50;
                break;
            }
            DeviceReset( io_Device ); break;             // else reset

        // PS/2 Mouse detected

        case 2:
            if( i_Byte == 0xFA ) {                       // ack
                ++io_Device.StateID;
                DeviceSendByte( io_Device, 0x03 );       // : 8 counts/mm
                break;
            }
            DeviceReset( io_Device ); break;             // else reset

        case 3:
            if( i_Byte == 0xFA ) {                       // ack
                ++io_Device.StateID;
                DeviceSendByte( io_Device, 0xE6 );       // set scaling: 1:1
                break;
            }
            DeviceReset( io_Device ); break;             // else reset

        case 4:
            if( i_Byte == 0xFA ) {                       // ack
                ++io_Device.StateID;
                DeviceSendByte( io_Device, 0xF3 );       // set sample rate
                break;
            }
            DeviceReset( io_Device ); break;             // else reset

        case 5:
            if( i_Byte == 0xFA ) {                       // ack
                ++io_Device.StateID;
                DeviceSendByte( io_Device, 200 );        // : 200 packets/sec (lower latency / less jitter... why not?)
                break;
            }
            DeviceReset( io_Device ); break;             // else reset

        case 6:
            if( i_Byte == 0xFA ) {                       // ack
                ++io_Device.StateID;
                DeviceSendByte( io_Device, 0xF4 );       // enable device
                break;
            }
            DeviceReset( io_Device ); break;             // else reset

        case 7:
             if( i_Byte == 0xFA ) {                      // ack
                 ++io_Device.StateID;
                 io_Device.Timeout = 0;
                 break;
             }
             DeviceReset( io_Device ); break;            // else reset

        case 8:
            if( i_Byte == 0xAA ) {                       // self-test passed: device re-connected? this _could_ be a valid data packet byte 0, but unlkely...
                DeviceReset( io_Device ); break;         // reset
            }
            if( i_Byte & 0b00001000 ) {                  // mouse streaming data packet byte 0
                io_Device.Aux3 = io_Device.Aux0;         // save previous packet header for comparison later
                io_Device.Aux0 = i_Byte;                 // packet header
                ++io_Device.StateID;
                break;
            }
            break;

        case 9:                                          // mouse streaming data packet byte 1
            io_Device.Aux1 = i_Byte;                     // X movement
            ++io_Device.StateID;
            break;

        case 10:                                         // mouse streaming data packet byte 2
            if( i_Byte || io_Device.Aux1 ) {             // process X,Y motion
                if( MouseEventIdx < MouseEventMax ) {
                    MouseEvent_t& t_MouseEvent = MouseEvent[MouseEventIdx];
                    t_MouseEvent.Code = Motion;

                    if( io_Device.Aux0 & 0b00010000 )
                        t_MouseEvent.X = (signed char)io_Device.Aux1;
                    else
                        t_MouseEvent.X = io_Device.Aux1;

                    if( io_Device.Aux0 & 0b00100000 )
                        t_MouseEvent.Y = -((signed char)i_Byte);    // invert Y
                    else
                        t_MouseEvent.Y = -i_Byte;                   // invert Y

                    t_MouseEvent.Z = 0;
                    t_MouseEvent.Aux = 0;

                    ++MouseEventIdx;
                }
            }
            unsigned char t_Btn;
            if( (t_Btn = (io_Device.Aux0 ^ io_Device.Aux3) & 0b00000001) ) {  // process Left button transition
                if( MouseEventIdx < MouseEventMax ) {
                    MouseEvent_t& t_MouseEvent = MouseEvent[MouseEventIdx];
                    if( t_Btn )
                        t_MouseEvent.Code = ButtonDown;
                    else
                        t_MouseEvent.Code = ButtonUp;

                    t_MouseEvent.X = 0;
                    t_MouseEvent.Y = 0;
                    t_MouseEvent.Z = 0;
                    t_MouseEvent.Aux = 1;

                    ++MouseEventIdx;
                }
            }
            if( (t_Btn = (io_Device.Aux0 ^ io_Device.Aux3) & 0b00000010) ) {  // process Left button transition
                if( MouseEventIdx < MouseEventMax ) {
                    MouseEvent_t& t_MouseEvent = MouseEvent[MouseEventIdx];
                    if( t_Btn )
                        t_MouseEvent.Code = ButtonDown;
                    else
                        t_MouseEvent.Code = ButtonUp;

                    t_MouseEvent.X = 0;
                    t_MouseEvent.Y = 0;
                    t_MouseEvent.Z = 0;
                    t_MouseEvent.Aux = 2;

                    ++MouseEventIdx;
                }
            }
            if( (t_Btn = (io_Device.Aux0 ^ io_Device.Aux3) & 0b00000100) ) {  // process Left button transition
                if( MouseEventIdx < MouseEventMax ) {
                    MouseEvent_t& t_MouseEvent = MouseEvent[MouseEventIdx];
                    if( t_Btn )
                        t_MouseEvent.Code = ButtonDown;
                    else
                        t_MouseEvent.Code = ButtonUp;

                    t_MouseEvent.X = 0;
                    t_MouseEvent.Y = 0;
                    t_MouseEvent.Z = 0;
                    t_MouseEvent.Aux = 3;

                    ++MouseEventIdx;
                }
            }
            io_Device.StateID = 8;
            break;

        // PS/2 Keyboard detected

        case 50: {
            if( i_Byte == 0xFA ) {                       // ack: it's a keyboard!
                ++io_Device.StateID;
                DeviceSendByte( io_Device, 0b00000010 ); // turn on numlock led
                io_Device.Timeout = 0;
                break;
            }
            if( i_Byte == 0xFE ) {                       // resend: it's a mouse!
                io_Device.StateID = 2;
                DeviceSendByte( io_Device, 0xE8 );       // Set Resolution
                break;
            }
            DeviceReset( io_Device );                    // else reset
        } break;

        case 51: {
            if( i_Byte == 0xAA ) {                       // self-test passed: device re-connected?
                DeviceReset( io_Device );                // reset
                break;
            }
            if( i_Byte == 0xF0 ) {                       // key break scancode
                io_Device.Aux0 |= KeyBreak;              // set "break" flag
                break;
            }
            if( i_Byte == 0xE0 ) {                       // extended scancode
                io_Device.Aux0 &= ~ExtBits;
                io_Device.Aux0 |= ExtE0;                 // set E0 "extended" flag
                break;
            }
            if( i_Byte == 0xE1 ) {                       // extended scancode used by Pause/Break key only
                io_Device.Aux0 &= ~ExtBits;
                io_Device.Aux0 |= ExtE1;                 // set E1 "extended" flag
                break;
            }
            if( i_Byte == 0x83 )                         // remap the odd 8-bit scancode for F7 key to a sane value in the normal 7-bit range
                i_Byte = 0x02;

            if( i_Byte < 0x80 ) {                        // key scancode
                if( KeybEventIdx < KeybEventMax ) {
                    KeybEvent_t& t_KeybEvent = KeybEvent[KeybEventIdx];

                    t_KeybEvent.Flags = io_Device.Aux0;
                    t_KeybEvent.Scan = i_Byte;
                    t_KeybEvent.Mods = io_Device.Aux1;
                    t_KeybEvent.Char = 0;

                    TranslateScanCode( t_KeybEvent );

                    io_Device.Aux0 = t_KeybEvent.Flags;
                    io_Device.Aux1 = t_KeybEvent.Mods;

                    io_Device.Aux0 &= ~(KeyBreak | ExtBits);

                    ++KeybEventIdx;
                }
            }
        } break;

        default: {
            if( i_Byte == 0xAA )                         // self-test passed: device re-connected?
                DeviceReset( io_Device );                // reset
        } break;
    }
}

void PS2Loop( chanend i_PS2Chan ) {
    unsigned int t_Timestamp;
    PS2Timer :> t_Timestamp;

    for( unsigned int i = 0; i < PS2DeviceCount; ++i )      // send periodic reset to probe for device
        SetTimeout( PS2State[i], StdTimeout );

    while( 1 ) {
        unsigned int t_Wait = PS2State[0].Wait;             // min
        for( unsigned int i = 1; i < PS2DeviceCount; ++i ) {
            if( PS2State[i].Wait < t_Wait )
                t_Wait = PS2State[i].Wait;
        }

        select {
            // wait for channel command
            case i_PS2Chan :> unsigned char t_ChanCommand: {
                unsigned char t_Header;
                switch( t_ChanCommand ) {
                    case 1: {                               // get PS2 Debug Log
                        t_Header = 1;                       // send PS2 Debug Log
                        i_PS2Chan <: t_Header;
#ifdef EnablePS2Log
                        i_PS2Chan <: (unsigned int)LogIdx;
                        for( unsigned int i = 0; i < LogIdx; ++i )
                            i_PS2Chan <: Log[i];

                        LogIdx = 0;
#else // EnablePS2Log
                        i_PS2Chan <: (unsigned int)0;
#endif // EnablePS2Log
                        t_Header = 0;                       // end transmission
                        i_PS2Chan <: t_Header;
                    } break;

                    case 2: {                               // get mouse events
                        t_Header = 1;                       // send mouse events for mouse #1
                        i_PS2Chan <: t_Header;

                        i_PS2Chan <: (unsigned int)MouseEventIdx;
                        for( unsigned int i = 0; i < MouseEventIdx; ++i )
                            i_PS2Chan <: MouseEvent[i];

                        MouseEventIdx = 0;

                        t_Header = 0;                       // end transmission
                        i_PS2Chan <: t_Header;
                    } break;

                    case 3: {
                        t_Header = 1;                       // send keyboard events for keyboard #1
                        i_PS2Chan <: t_Header;

                        i_PS2Chan <: (unsigned int)KeybEventIdx;
                        for( unsigned int i = 0; i < KeybEventIdx; ++i )
                            i_PS2Chan <: KeybEvent[i];

                        KeybEventIdx = 0;

                        t_Header = 0;                       // end transmission
                        i_PS2Chan <: t_Header;
                    } break;
                }
            } break;

            // wait for next timed event
            case PS2Timer when timerafter( t_Timestamp + t_Wait ) :> t_Timestamp: {

                for( unsigned int i = 0; i < PS2DeviceCount; ++i ) {
                    PS2State_t& t_State = PS2State[i];
                    if( t_State.Timeout && (t_Timestamp >= t_State.Timeout) ) {
                        t_State.Timeout = 0;
                        OnDeviceEvent( t_State, TimeoutEvent );
                    }
                }

                if( t_Wait != StdSleep ) {
                    for( unsigned int i = 0; i < PS2DeviceCount; ++i ) {
                        PS2State_t& t_State = PS2State[i];
                        if( t_Wait == t_State.Wait ) {
                            if( t_State.ClkLo ) {
                                int t_Bit;
                                PS2Port[i].DataIn :> t_Bit;
                                t_State.ClkLo = 0;

                                if( PS2Framer_OnDataBit( i, t_Bit ^ PS2_INV, t_Timestamp ) )
                                    OnDeviceEvent( t_State, PS2Framer_ReadByte( i ) );
                            }
                            t_State.Wait = StdSleep;
                        }
                    }
                }
            } break;

            // wait for transition on PS2 Clk pin
            case( unsigned int i = 0; i < PS2DeviceCount; ++i ) PS2Port[i].ClkIn when pinsneq( PS2State[i].ClkLevel ) :> PS2State[i].ClkLevel: {
                PS2State_t& t_State = PS2State[i];
                if( t_State.ClkLevel == PS2_LO ) {
                    t_State.ClkLo = 1;
                    PS2Timer :> t_Timestamp;
                    t_State.Wait = 2000;   // 20 �s
                }
            } break;
        }
    }
}

void PS2Init( clock i_Clock ) {
    for( unsigned int i = 0; i < PS2DeviceCount; ++i ) {
        configure_in_port( PS2Port[i].ClkIn, i_Clock );
        configure_in_port( PS2Port[i].DataIn, i_Clock );
        configure_out_port( PS2Port[i].ClkOut, i_Clock, PS2_HI );
        configure_out_port( PS2Port[i].DataOut, i_Clock, PS2_HI );
    }

#ifdef EnableScopeTrig
    configure_out_port( ScopeTrigOut, i_Clock, 0 );
#endif // EnableScopeTrig

    // initialize PS2 states
    for( unsigned int i = 0; i < PS2DeviceCount; ++i ) {
        PS2State[i].DeviceIndex = i;
        PS2State[i].Wait = StdSleep;
        PS2State[i].Timeout = 0;
        PS2State[i].ClkLevel = PS2_HI;
        PS2State[i].ClkLo = 0;
        PS2State[i].StateID = 0;
        PS2State[i].Aux0 = 0;
        PS2State[i].Aux1 = 0;
        PS2State[i].Aux2 = 0;
        PS2State[i].Aux3 = 0;
    }

    PS2Framer_construct( 16, 1 );  // Mouse, 16 byte buffer, ignore errors (return Index = 0)
    PS2Framer_construct( 16, 1 );  // Keyboard, 16 byte buffer, ignore errors (return Index = 1)
}

//void MouseLoop( streaming chanend i_MouseConsumer ) {
//    int X = 0;
//    int Y = 0;
//
//    unsigned char t_LastHeader = 0;
//
//    MouseEvent_t PositionEvent;
//    PositionEvent.Type = 1;
//
//    MouseEvent_t MouseEvents[16];
//    int EventCount = 0;
//
//    while( 1 ) {
//        unsigned char t_Header;
//        if( ReceiveByte( MousePort, t_Header ) == 0 ) {
//            unsigned char t_XMov;
//            if( (t_Header & 0b00001000) && (ReceiveByte( MousePort, t_XMov ) == 0) ) {
//                unsigned char t_YMov;
//                if( ReceiveByte( MousePort, t_YMov ) == 0 ) {
//                    if( t_Header & 0b00010000 )
//                        X += (char)t_XMov;
//                    else
//                        X += t_XMov;
//
//                    if( t_Header & 0b00100000 )
//                        Y += (char)t_YMov;
//                    else
//                        Y += t_YMov;
//
//                    if( (EventCount < 16) && ((t_Header ^ t_LastHeader) & 0b00000111) ) {
//                        if( (t_Header ^ t_LastHeader) & 0b00000001 ) {
//                            if( t_Header & 0b00000001 )
//                                MouseEvents[EventCount].Type = 3; // left button down
//                            else
//                                MouseEvents[EventCount].Type = 2; // left button up
//                        }
//                        if( (t_Header ^ t_LastHeader) & 0b00000010 ) {
//                            if( t_Header & 0b00000010 )
//                                MouseEvents[EventCount].Type = 5; // right button down
//                            else
//                                MouseEvents[EventCount].Type = 4; // right button up
//                        }
//                        if( (t_Header ^ t_LastHeader) & 0b00000100 ) {
//                            if( t_Header & 0b00000100 )
//                                MouseEvents[EventCount].Type = 9; // middle button down
//                            else
//                                MouseEvents[EventCount].Type = 8; // middle button up
//                        }
//
//                        MouseEvents[EventCount].X = X;
//                        MouseEvents[EventCount].Y = Y;
//                        EventCount++;
//
//                        t_LastHeader = t_Header;
//                    }
//                }
//            }
//        }
//    }
//}
