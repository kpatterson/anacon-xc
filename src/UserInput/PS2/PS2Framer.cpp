/*
 * UserInput/PS2/PS2Framer.cpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Feb 7, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */

#include "UserInput/PS2/PS2Framer.hpp"

#include <vector>


std::vector<PS2Framer*> PS2Framers;


PS2Framer::PS2Framer( unsigned char i_BufSize, bool i_IgnoreErrors )
: m_LastTimestamp( 0 )
, m_Word( 0 )
, m_OutWord( Err_Empty )
, m_Bits( 0 )
, m_IgnoreErrors( i_IgnoreErrors )
{
    Resync();
}

bool PS2Framer::OnDataBit( bool i_Bit, unsigned int i_Timestamp ) {
    if( i_Timestamp < m_LastTimestamp + 3000 ) { // 30 �s min
//        PeriodTooShort();
        m_LastTimestamp = i_Timestamp;
        return false;
    }

    if( i_Timestamp > m_LastTimestamp + 15000 ) // 150 �s max
        Resync();

    m_Word >>= 1;
    m_Word |= (i_Bit << 15);
    ++m_Bits;

    m_LastTimestamp = i_Timestamp;

    if( m_Bits == 11 ) {
        StoreWord();
        return true;
    }
    return false;
}

void PS2Framer::StoreWord() {
    unsigned char t_Err = Err_None;

    if( m_Bits < 11 )
        t_Err |= Err_Underflow;

    if( (m_Bits > 11) || (m_Word & 0b0000000000011111) )
        t_Err |= Err_Overflow;        // bit count overflow

    m_Word >>= 5;

    if(  m_Word & 0b0000010000000000 )
        t_Err |= Err_BadStartBit;

    if( (m_Word & 0b0000000000000001) != 0x01 )
        t_Err |= Err_BadStopBit;

    m_Word >>= 1;
    bool t_CheckParity = m_Word & 0x01;

    unsigned char t_Byte = m_Word;

    bool t_Parity = true;
    for( unsigned int i = 0; i < 8; ++i ) {
        m_Word >>= 1;
        t_Parity ^= m_Word & 0x1;
    }

    if( t_Parity != t_CheckParity )
        t_Err |= Err_Parity;

    if( (t_Err == Err_None) || m_IgnoreErrors ) {
        m_OutWord = t_Byte | t_Err;
        Resync();
    }
}


extern "C" {

    unsigned int PS2Framer_construct( unsigned char i_BufSize, int i_IgnoreErrors ) {
        PS2Framers.push_back( new PS2Framer( i_BufSize, i_IgnoreErrors ) );
        return PS2Framers.size() - 1;
    }

    int PS2Framer_OnDataBit( unsigned int i_Index, int i_Bit, unsigned int i_Timestamp ) {
        return PS2Framers[i_Index]->OnDataBit( i_Bit, i_Timestamp );
    }

    unsigned short PS2Framer_ReadByte( unsigned int i_Index ) {
        return PS2Framers[i_Index]->ReadByte();
    }

    void PS2Framer_Reset( unsigned int i_Index ) {
        PS2Framers[i_Index]->Reset();
    }
}
