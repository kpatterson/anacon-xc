#ifndef PS2FRAMER_HPP_
#define PS2FRAMER_HPP_
/*
 * UserInput/PS2/PS2Framer.hpp
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Feb 7, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */


class PS2Framer {
private:
    enum Err_e {
        Err_None          = 0x0000
        , Err_Underflow   = 0x0100
        , Err_Overflow    = 0x0200
        , Err_BadStartBit = 0x0400
        , Err_BadStopBit  = 0x0800
        , Err_Parity      = 0x1000
        , Err_Empty       = 0x2000
    };

public:
    PS2Framer( unsigned char i_BufSize, bool i_IgnoreErrors );

//    ~PS2Framer();

    void SetIgnoreErrors( bool i_IgnoreErrors = true ) {
        m_IgnoreErrors = i_IgnoreErrors;
    }

    bool OnDataBit( bool i_Bit, unsigned int i_Timestamp );

    unsigned short ReadByte() { unsigned short t_Word = m_OutWord; m_OutWord = Err_Empty; return t_Word; }

    void Reset() { m_OutWord = Err_Empty; Resync(); }

private:
    void Resync() { m_Word = 0; m_Bits = 0; }

    void StoreWord();

private:
    unsigned int m_LastTimestamp;

    unsigned short m_Word;
    unsigned short m_OutWord;

    unsigned char m_Bits;

    bool m_IgnoreErrors;
};


#endif /* PS2FRAMER_HPP_ */
