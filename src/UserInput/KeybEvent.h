#ifndef KEYBEVENT_H_
#define KEYBEVENT_H_
/*
 * UserInput/KeybEvent.h
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Feb 11, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */


typedef struct KeybEvent_t {
    unsigned char Flags;
    unsigned char Scan;
    unsigned char Mods;
    unsigned char Char;
} KeybEvent_t;

enum KeybFlags_e {
      RawScanCode = 0b10000000
    , KeyBreak    = 0b01000000
    , ExtBits     = 0b00110000
    , ExtE0       = 0b00100000
    , ExtE1       = 0b00110000
    , InsertMode  = 0b00001000
    , ScrollLock  = 0b00000100
    , NumLock     = 0b00000010
    , CapsLock    = 0b00000001
};

enum KeybMods_e {
      Shift       = 0b11000000
    , LeftShift   = 0b10000000
    , RightShift  = 0b01000000
    , Ctrl        = 0b00110000
    , LeftCtrl    = 0b00100000
    , RightCtrl   = 0b00010000
    , Alt         = 0b00001100
    , LeftAlt     = 0b00001000
    , RightAlt    = 0b00000100
    , Meta        = 0b00000011
    , LeftMeta    = 0b00000010
    , RightMeta   = 0b00000001
};


#endif /* KEYBEVENT_H_ */
