#ifndef MOUSEEVENT_H_
#define MOUSEEVENT_H_
/*
 * UserInput/MouseEvent.h
 * xcore-anacon - An analog video console for XMOS xCORE devices
 *
 *  Created on: Feb 10, 2017
 *      Author: Kevin H. Patterson
 *
 * Copyright (C) 2015-2017 Kevin H. Patterson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Alternative licensing is available:
 * Contact kevpatt _at_ khptech _dot_ com for more information.
 */


typedef struct MouseEvent_t {
    short X;
    short Y;
    short Z;
    char Aux;
    unsigned char Code;
} MouseEvent_t;

enum MouseEventCode_e {
    None = 0
    , Motion = 1
    , ButtonDown = 2
    , ButtonUp = 3
    , Scroll = 4
};


#endif /* MOUSEEVENT_H_ */
